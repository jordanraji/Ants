# -*- coding: utf-8 -*-
from __future__ import print_function
from random import randint
import random
import time
import os

NUM_ANTS = 200
NUM_ITERATIONS = 1000
w, h = 40, 40;
matrix = [[ [0,0,0] for x in range(w)] for y in range(h)]

"""
[A,B,C]
A -> 0 = vacio, 1 = nido
B -> k = k unidades de comida
C -> k = k nivel de feromona
"""

nest = (randint(0,w-1), randint(0,h-1))
# print "Nido:", nest
matrix[nest[1]][nest[0]][0] = 1
matrix[nest[1]][nest[0]][2] = 9

pieces_of_food = randint(1,(w*h)/3);
for i in range(0,pieces_of_food):
    food = (randint(0,w-1),randint(0,h-1))
    matrix[food[1]][food[0]][1] = randint(1,9)

def updatePheromone(matrix):
    for row in matrix:
        for value in row:
            if (value[0]!= 1 and value[2] > 0):
                value[2] -= 1

# def printMatrix(matrix):
#     for value in matrix:
#         print value

def printMatrix(matrix):
    for row in matrix:
        for column in row:
            if column[2] == 8:
                print ("8", end=' ')
            else:
                print ("_", end=' ')
        print('\n', end='')

class Ant:
    current_position = [0, 0]
    has_food = 0 # 0 -> no tiene comida, 1 -> tiene comida
    global nest

    def __init__(self):
        # self.current_position = [randint(0,w-1), randint(0,h-1)]
        self.current_position = list(nest)

    def moveLeft(self):
        if self.current_position[0] != 0:
            self.current_position[0] -= 1

    def moveRight(self):
        if self.current_position[0] != w-1:
            self.current_position[0] += 1

    def moveUp(self):
        if self.current_position[1] != 0:
            self.current_position[1] -= 1

    def moveDown(self):
        if self.current_position[1] != h-1:
            self.current_position[1] += 1

    def catchFood(self, matrix):
        if (matrix[self.current_position[1]][self.current_position[0]][0] != 1
            and matrix[self.current_position[1]][self.current_position[0]][1] > 0
            and self.has_food == 0):
            matrix[self.current_position[1]][self.current_position[0]][1] -= 1
            self.has_food = 1
            print ("cogió comida")

    def leaveFood(self, matrix):
        if (matrix[self.current_position[1]][self.current_position[0]][0] == 1
            and self.has_food == 1):
            matrix[self.current_position[1]][self.current_position[0]][1] += 1
            self.has_food = 0
            print ("dejó comida")

    def leavePheromone(self, matrix):
        matrix[self.current_position[1]][self.current_position[0]][2] = 9

    def MoveAnt(self, matrix):
        where_is_pheromone = [0,0,0,0]
        """
        |0|N|0|
        |W|H|E| -> [N,E,S,W]
        |0|S|0|
        """
        # print "Mi posicion:",self.current_position

        neighbor_north = (self.current_position[0], self.current_position[1] - 1)
        if (neighbor_north[1]<0):
            neighbor_north = None
        # print "Vecino del norte:", neighbor_north
        neighbor_south = (self.current_position[0], self.current_position[1] + 1)
        if (neighbor_south[1]>h-1):
            neighbor_south = None
        # print "Vecino del sur:", neighbor_south
        neighbor_west = (self.current_position[0] - 1 , self.current_position[1])
        if (neighbor_west[0]<0):
            neighbor_west = None
        neighbor_east = (self.current_position[0] + 1 , self.current_position[1])
        # print "Vecino del oeste:", neighbor_west
        if (neighbor_east[0]>w-1):
            neighbor_east = None
        # print "Vecino del este:", neighbor_east

        if (neighbor_north != None and
            matrix[neighbor_north[1]][neighbor_north[0]][2] > 0):
            where_is_pheromone[0] = 1;
        if (neighbor_south != None and
            matrix[neighbor_south[1]][neighbor_south[0]][2] > 0):
            where_is_pheromone[2] = 1;
        if (neighbor_east != None and
            matrix[neighbor_east[1]][neighbor_east[0]][2] > 0):
            where_is_pheromone[1] = 1;
        if (neighbor_west != None and
            matrix[neighbor_west[1]][neighbor_west[0]][2] > 0):
            where_is_pheromone[3] = 1;

        # count = 0
        # for value in where_is_pheromone:
        #     if value == 1:
        #         count += 1
        #
        # print "C:",count
        # probability = float(count)/4.0
        # print "P:",probability
        # value_random = random.uniform(0, 1)
        # print "R:",value_random

        p = randint(0,3)
        # print "pos:",p
        # matrix[self.current_position[1]][self.current_position[0]][2]-=1
        if p == 0:
            self.moveUp()
        elif p == 1:
            self.moveRight()
        elif p == 2:
            self.moveDown()
        elif p == 3:
            self.moveLeft()
        self.leavePheromone(matrix)
        self.catchFood(matrix)
        self.leaveFood(matrix)

        # if where_is_pheromone == [0,0,0,0]:
        #     p = randint(0,3)
        #     print "pos:",p
        #     matrix[self.current_position[1]][self.current_position[0]][2]-=1
        #     if p == 0:
        #         self.moveUp()
        #     elif p == 1:
        #         self.moveRight()
        #     elif p == 2:
        #         self.moveDown()
        #     elif p == 3:
        #         self.moveLeft()
        #     self.leavePheromone(matrix)
        # else:
        #     p = randint(0,3)
        #     while where_is_pheromone[p]==0:
        #         p +=1
        #         if p == 4:
        #             p = 0
        #     print "pos:",p
        #     matrix[self.current_position[1]][self.current_position[0]][2]-=1
        #     if p == 0:
        #         self.moveUp()
        #     elif p == 1:
        #         self.moveRight()
        #     elif p == 2:
        #         self.moveDown()
        #     elif p == 3:
        #         self.moveLeft()
        #     self.leavePheromone(matrix)

        # return where_is_pheromone

    # def moveAnt(self, positions):



# hormiga1 = Ant()
# hormiga2 = Ant()
# hormiga3 = Ant()
# hormiga4 = Ant()
# hormiga5 = Ant()
# hormiga6 = Ant()
# hormiga7 = Ant()
# hormiga8 = Ant()
# hormiga9 = Ant()
# hormiga10 = Ant()
#
# hormiga1.leavePheromone(matrix)
# hormiga2.leavePheromone(matrix)
# hormiga3.leavePheromone(matrix)
# hormiga4.leavePheromone(matrix)
# hormiga5.leavePheromone(matrix)
# hormiga6.leavePheromone(matrix)
# hormiga7.leavePheromone(matrix)
# hormiga8.leavePheromone(matrix)
# hormiga9.leavePheromone(matrix)
# hormiga10.leavePheromone(matrix)

my_ants = []



for i in range (NUM_ANTS):
    my_ants.append(Ant())

for i in range (NUM_ANTS):
    my_ants[i].leavePheromone(matrix)

os.system('clear')
printMatrix(matrix)
print ("Nido: ", nest)
print ("Cantidad de comida en el nido:" + str(matrix[nest[1]][nest[0]][1]))
os.system('clear')
iteration = 0
while iteration < NUM_ITERATIONS:
    # hormiga1.MoveAnt(matrix)
    # hormiga2.MoveAnt(matrix)
    # hormiga3.MoveAnt(matrix)
    # hormiga4.MoveAnt(matrix)
    # hormiga5.MoveAnt(matrix)
    # hormiga6.MoveAnt(matrix)
    # hormiga7.MoveAnt(matrix)
    # hormiga8.MoveAnt(matrix)
    # hormiga9.MoveAnt(matrix)
    # hormiga10.MoveAnt(matrix)
    for i in range (NUM_ANTS):
        my_ants[i].MoveAnt(matrix)

    updatePheromone(matrix)
    printMatrix(matrix)
    print ("Nido: ", nest)
    print ("Cantidad de comida en el nido:" + str(matrix[nest[1]][nest[0]][1]))
    time.sleep(0.25)
    os.system('clear')
    iteration += 1
